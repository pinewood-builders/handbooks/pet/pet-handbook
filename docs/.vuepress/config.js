module.exports = ctx => ({
  head: [
    ["link", { rel: "icon", href: "/PET-Logo.png" }],
    ["link", { rel: "manifest", href: "/manifest.json" }],
    ["meta", { name: "theme-color", content: "#fca903" }]
  ],
  dest: "public/",
  title: "Pinewood Emergency Team",
  description: "The unofficial PET Handbook",
  themeConfig: {
    sidebarDepth: 1,
    repo: "https://gitlab.com/S-FGR/pinewood-builders/groups/pet/pet-handbook",
    editLinks: true,
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: "red",
      disableThemeIgnore: true,
    },
    algolia: 
       {
          apiKey: "4c6e6affed80cfe0c8d529795755c921",
          indexName: "pinewood-builders_pet",
        }
      ,

    docsDir: "docs/",
    logo: "/PET-Logo.png",
    smoothScroll: true,
    nav: [
      {
        text: "Home",
        link: "/",
      },
      {
        text: 'Pinewood',
        items: [{
          text: 'Pinewood Homepage',
          link: 'https://pinewood-builders.com'
        },
          {
            text: 'PBST Handbook',
            link: 'https://pbst.pinewood-builders.com'
          },
          {
            text: 'TMS Handbook',
            link: 'https://tms.pinewood-builders.com'
          },
        ]
      },
    ],

      sidebar: [{
        collapsable: true,
        title: 'PET Handbook',
        children: ['/handbook/'],
      }
      ],
  },
  plugins: [
    [
      "@vuepress/pwa",
      {
        serviceWorker: true,
        updatePopup: true,
      },
    ],
    [
      "seo",
      {
        siteTitle: (_, $site) => $site.title,
        title: $page => $page.title,
        description: $page => $page.frontmatter.description,
        author: (_, $site) => $site.themeConfig.author,
        tags: $page => $page.frontmatter.tags,
        twitterCard: _ => 'summary_large_image',
        type: $page => ['articles', 'posts', 'blog'].some(folder => $page.regularPath.startsWith('/' + folder)) ? 'article' : 'website',
        url: (_, $site, path) => ($site.themeConfig.domain || '') + path,
        image: ($page, $site) => $page.frontmatter.image && (($site.themeConfig.domain && !$page.frontmatter.image.startsWith('http') || '') + $page.frontmatter.image),
        publishedAt: $page => $page.frontmatter.date && new Date($page.frontmatter.date),
        modifiedAt: $page => $page.lastUpdated && new Date($page.lastUpdated),
      },
    ],
  ],
});
