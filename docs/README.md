---
home: true
heroImage: /PET-Logo.png
actionText: Read the handbook →
actionLink: /handbook/
footer: "Last Modification of Rules: 29/11/2020"
forceTheme: green
---

::: danger Shutdown Notice
Due to maintanace reasons, this website will shutdown at the end of the year.
:::

::: warning WARNING!
Please read through all of the rules very carefully. Any changes to the rules will be made clear. The rules have been categorized into who they apply to, however some rules may apply to more than one group of PET Members, in which case it will be made clear. Punishments for breaking a rule depend on what rule and the severity. Punishments can range from a simple warning, demotion, or even blacklist from PET.
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a>
<div>
  <a href="https://www.jetbrains.com/?from=Pinewood-Builders">
    <img src="jetbrains.png" width="150"/>
  </a>
</div>
</center>
